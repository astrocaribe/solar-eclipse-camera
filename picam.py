#!/usr/bin/env python

from picamera import PiCamera
from time import sleep
import select
import sys


def cam_preview(camObj):
    # Print camera modes information
    print("gains: ", camObj.awb_gains)
    print("iso: ", camObj.iso)
    print("exposure_speed: ", camObj.exposure_speed)
    print("shutter_speed: ", camObj.shutter_speed)

    # Wait for the automatic gain control to settle
    camObj.start_preview()
    sleep(2)
    return camObj


def capture_image(camObj, iterator):
    # Capture a photo given camera object and an iterator
    filename = "/home/pi/projects/solar-eclipse-camera/assets/img{0:04d}.jpg".format(iterator)
    print("Capturing " + filename + "...")
    camObj.capture(filename)
    camObj.stop_preview()
    print()

    return camObj


# Initial setup
camera = PiCamera()
# camera.iso = 800
camera.resolution = '1080p'
camera.exposure_mode = 'auto'
camera.shutter_speed = 0
camera.awb_mode = 'auto'
camera.meter_mode = 'backlit'


# Loop camera capture every 10 secs
for ii in range(1, 1500):
    cam_preview(camera)
    capture_image(camera, ii)
    sleep(10)  # wait 10 seconds


print("Cam campture done!")
